# Ansible Role: mtusseau.age

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Installs and configures [age](https://github.com/FiloSottile/age).

## Requirements

See [meta/main.yml](meta/main.yml)

## Role Variables

See [defaults/main.yml](defaults/main.yml)

## Dependencies

See [meta/main.yml](meta/main.yml)

## Example Playbook

```yml
- hosts: servers
  roles:
    - mtusseau.age
```

## License

MIT

## Notes

* I use Docker images build by [Jeff Geerling](https://github.com/geerlingguy) for CI
* This ansible role is strongly inspired by roles developed by [Andrew Rothstein](https://github.com/andrewrothstein)

## Author Information

Maxime Tusseau maxime.tusseau@gmail.com
